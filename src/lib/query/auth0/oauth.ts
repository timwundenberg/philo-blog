import AsyncLock from 'async-lock';

const lock = new AsyncLock();

const jwt = {
    access_token: '',
    expires_in: 0,
    timestamp_seconds_fetched: 0
};

function checkAndResetJwt() {
    if (jwt.access_token == '') return;
    if (
        jwt.timestamp_seconds_fetched + jwt.expires_in + 60 /*small buffer*/ <
        Math.floor(Date.now() / 1000)
    ) {
        jwt.access_token = '';
        jwt.expires_in = 0;
        jwt.timestamp_seconds_fetched = 0;
    }
}

export async function getAccessToken(): Promise<string> {
    return lock.acquire('', async () => {
        checkAndResetJwt();
        if (jwt.access_token) return jwt.access_token;

        const body = `{
        "client_id": "${process.env.AUTH0_MGMT_CLIENT_ID}",
        "client_secret": "${process.env.AUTH0_MGMT_CLIENT_SECRET}",
        "audience": "${process.env.AUTH0_MGMT_PATH}/",
        "grant_type": "client_credentials"
    }`;

        console.log('fetch access token ');
        const respone = await fetch(
            `${process.env.AUTH0_ISSUER_BASE_URL}/oauth/token`,
            {
                method: 'POST',
                headers: {
                    'content-type': 'application/json'
                },
                body: body
            }
        );

        const { access_token, expires_in } = await respone.json();

        jwt.access_token = access_token;
        jwt.timestamp_seconds_fetched = Math.floor(Date.now() / 1000);
        jwt.expires_in = expires_in;

        return jwt.access_token;
    });
}
