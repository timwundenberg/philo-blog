import { Claims } from '@auth0/nextjs-auth0';
import AsyncLock from 'async-lock';
import { getAccessToken } from './oauth';

export async function getUserById(id: string): Promise<Claims> {
    const response = await fetch(
        `${process.env.AUTH0_MGMT_PATH}/users/${encodeURIComponent(id)}`,
        {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + (await getAccessToken())
            }
        }
    );
    if (response.status != 200) {
        console.error(response);
        throw new Error('Error while fetching data!');
    }

    const content = await response.json();
    return content as Claims;
}

const lock = new AsyncLock();
const storage = new Map<string, Claims>();

export async function getUsername(userId: string): Promise<string> {
    return await lock.acquire(userId, async () => {
        if (storage.has(userId)) {
            const name = storage.get(userId)?.nickname;
            return name ? name : '';
        }

        const user = await getUserById(userId);
        storage.set(userId, user);
        return user.nickname ?? '';
    });
}
