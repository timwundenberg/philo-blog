import { useMutation, useQuery, useQueryClient } from 'react-query';
import {
    BlogPostContent,
    BlogPostContentEdit,
    BlogPostFeedElement
} from '../../pages/api/models/blog-post-dto';
import { isUUID } from '../type-utils';

export function usePosts() {
    return useQuery<BlogPostFeedElement[], Error>('posts', async () => {
        const response = await fetch(`/api/forum`);
        if (response.status != 200)
            throw new Error('Error while fetching data!');
        return (await response.json()) as BlogPostFeedElement[];
    });
}

export function usePostById(id: string) {
    const isNew = id == 'new';
    if (!isUUID(id) && !isNew) throw new Error('Invalid Argument');

    return useQuery<BlogPostContent, Error>(
        ['posts', id],
        async () => {
            const response = await fetch(`/api/forum/${id}`);
            if (response.status != 200)
                throw new Error('Error while fetching data!');
            return (await response.json()) as BlogPostContent;
        },
        { enabled: !isNew }
    );
}

export async function useMutateSavePost() {
    const queryClient = useQueryClient();

    return useMutation(
        async (data: BlogPostContentEdit) => {
            if (!isUUID(data.id) || data.content == null)
                throw new Error('Invalid Argument');

            const response = await fetch(`/api/forum/${data.id}`, {
                method: 'PUT',
                body: JSON.stringify(data)
            });

            if (response.status != 200) throw new Error('Error saving post');
        },
        {
            onSuccess: () => void queryClient.invalidateQueries('posts'),
            onError: (e) => console.error(e)
        }
    );
}

export async function useMutateDeletePost() {
    const queryClient = useQueryClient();

    return useMutation(
        async (id: string) => {
            if (!isUUID(id)) throw new Error('Invalid Argument');

            const response = await fetch(`/api/forum/${id}`, {
                method: 'DELETE'
            });

            if (response.status != 200) throw new Error('Error saving post');
        },
        {
            onSuccess: () => void queryClient.invalidateQueries('posts'),
            onError: (e) => console.error(e)
        }
    );
}
