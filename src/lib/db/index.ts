import { Pool } from 'pg';
import { runMigrations } from './run-migrations';

const pool = new Pool({
    application_name: 'next.js/philo-blog'
    //other parameters are configured in .env
});

pool.on('error', (err) => {
    console.error('Unexpected error on idle client', err);
    // process.exit(-1);
});

await runMigrations(pool);

// Idially the pool is closed on application-exit.
// Unfortanatly, it's (currently) not possible in next.js to run cleanup-code when the node-js server is closed.
// Lukily, the connection is droped instantly by the postgresql server.
// await pool.end();

export { pool };
