import { Pool, PoolClient } from 'pg';
import type * as s from 'zapatos/schema';
import * as db from 'zapatos/db';
import migrations from './migrations';

async function createMigrationTable(pool: Pool) {
    await pool.query(`
        CREATE TABLE IF NOT EXISTS public.migrations
        (
            version varchar(50),
            date_executed timestamptz,
            PRIMARY KEY (version)
        );
    `);
}

const hasRun = async (version: string, pool: Pool): Promise<boolean> => {
    const migrationSearch: s.migrations.Whereable = {
        version: version
    };

    const migrations = await db.sql<
        s.migrations.SQL,
        s.migrations.Selectable[]
    >`
        SELECT *
        FROM ${'migrations'}
        WHERE ${migrationSearch}
    `.run(pool);

    return migrations.length == 1;
};

const updateLastRun = async (version: string, client: PoolClient) => {
    await db
        .insert('migrations', {
            version: version,
            date_executed: db.toString(new Date(), 'timestamptz')
        })
        .run(client);
};

const runMigrations = async (pool: Pool) => {
    await createMigrationTable(pool);
    for (const migration of migrations) {
        if (await hasRun(migration.version, pool)) continue;

        //TODO: on first migration run backup of Database!

        await db.transaction(
            pool,
            db.IsolationLevel.Serializable,
            async (txnClient) => {
                await txnClient.query(migration.sql);
                await updateLastRun(migration.version, txnClient);

                return true;
            }
        );
    }
};

export { runMigrations };
