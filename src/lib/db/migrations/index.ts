import m1 from './2022_03_02_initial.sql';

const migrations = [{ version: '2022_03_02_initial', sql: m1 }];

export default migrations;
