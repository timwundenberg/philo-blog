DROP TABLE IF EXISTS public.users;


--DROP TABLE IF EXISTS public.posts;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS public.posts
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    user_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    slug character varying(255) COLLATE pg_catalog."default" NOT NULL,
    content character varying COLLATE pg_catalog."default",
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    published_at timestamp with time zone,
    CONSTRAINT posts_pkey PRIMARY KEY (id),
    CONSTRAINT slug_unique UNIQUE (slug)
)


-- example data
-- insert into posts(user_id, slug, "content")
-- values ('user_25qIRjfGUhOJ47ScsVUrkMdeNxb', 'my-test-3', '# hello'),
-- ('user_25qIRjfGUhOJ47ScsVUrkMdeNxb', 'my-test-5', '# hello');