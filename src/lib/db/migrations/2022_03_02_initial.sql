
CREATE TABLE IF NOT EXISTS public.users
(
    id uuid NOT NULL,
    display_name character varying(50) COLLATE pg_catalog."default",
    email character varying COLLATE pg_catalog."default",
    password character varying COLLATE pg_catalog."default",
    CONSTRAINT users_pkey PRIMARY KEY (id)
)