import type { NextPage } from 'next';
import Container from '../components/form/container';

const Home: NextPage = () => {
    return (
        <Container>
            <h2>Home</h2>
            <ul>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
                <li>List Content</li>
            </ul>
        </Container>
    );
};

export default Home;
