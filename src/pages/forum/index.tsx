import React from 'react';
import PostFeed from '../../components/post/post-feed';
import Container from '../../components/form/container';

const BlogPost = () => {
    return (
        <Container>
            <PostFeed />
        </Container>
    );
};

export default BlogPost;
