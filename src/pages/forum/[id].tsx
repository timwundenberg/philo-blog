import { NextPage } from 'next';
import { useRouter } from 'next/router';
import React, { useLayoutEffect, useState } from 'react';
import { EditMode } from '../../components/post/post-helper';
import BlogPostManager from '../../components/post/post-manager';

const Blog: NextPage = () => {
    const id = useRouter().query.id as string;
    const [mode, setMode] = useState(EditMode.Edit);

    useLayoutEffect(() => {
        setMode(id == 'new' ? EditMode.Insert : EditMode.Edit);
    }, [id]);

    if (!id) return null;
    return <BlogPostManager mode={mode} postId={id} />;
};

export default Blog;
