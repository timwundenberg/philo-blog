import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { UserProvider } from '@auth0/nextjs-auth0';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import Head from 'next/head';
import Layout from '../components/layout/layout';

const queryClient = new QueryClient();

function MyApp({ Component, pageProps }: AppProps) {
    return (
        <QueryClientProvider client={queryClient}>
            <UserProvider>
                <Head>
                    {/* <link rel="icon" href="/favicon.ico" /> */}
                    <link rel="icon" type="image/png" href="/favicon.png" />
                    <link rel="icon" type="image/svg+xml" href="/favicon.svg" />
                    <title key="title">Philoferie</title>
                </Head>
                <Layout>
                    <Component {...pageProps} />
                </Layout>
                <ReactQueryDevtools initialIsOpen={false} />
            </UserProvider>
        </QueryClientProvider>
    );
}

export default MyApp;
