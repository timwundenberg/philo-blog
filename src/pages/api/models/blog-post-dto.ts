export interface BlogPostContent {
    id: string;
    userId: string;
    slug: string;
    content: string;
    published: boolean;
}

export interface BlogPostContentEdit {
    id: string;
    slug: string;
    content: string;
    published: boolean;
}

export interface BlogPostFeedSearch {
    page: number;
    pageSize: number;
    userId?: string;
}

export interface BlogPostFeedElement {
    id: string;
    userId: string;
    username: string;
    slug: string;
    publishedAt: string | null;
    updatedAt: string | null;
}
