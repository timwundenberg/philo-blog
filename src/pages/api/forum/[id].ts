import type { NextApiRequest, NextApiResponse } from 'next';
import { pool } from '../../../lib/db';
import * as db from 'zapatos/db';
import { isUUID } from '../../../lib/type-utils';
import { BlogPostContent, BlogPostContentEdit } from '../models/blog-post-dto';
import { Claims, getSession } from '@auth0/nextjs-auth0';

async function getPostById(
    req: NextApiRequest,
    res: NextApiResponse<BlogPostContent | null>
) {
    const { id } = req.query;
    if (typeof id != 'string' || !isUUID(id)) return res.status(400).send(null);

    const post = await db.selectOne('posts', { id }).run(pool);
    if (!post) return res.status(404).send(null);

    const result: BlogPostContent = {
        id: post.id,
        userId: post.user_id,
        slug: post.slug,
        content: post.content ? post.content : '',
        published: post.published_at ? true : false
    };

    res.status(200).json(result);
}

async function upsertPostById(req: NextApiRequest, res: NextApiResponse) {
    const { id } = req.query;

    const session = getSession(req, res);
    let user: Claims = [];
    if (session) user = session.user;

    const { slug, content, published } = JSON.parse(
        req.body
    ) as BlogPostContentEdit;

    if (typeof id != 'string' || !isUUID(id) || !content || !slug)
        return res.status(400).send(null);

    const post = await db.selectOne('posts', { id: id }).run(pool);
    if (!user) return res.status(401).send(null);
    if (post && post.user_id != user.sub) return res.status(401).send(null);

    if (post) {
        let published_at: Date | null;
        if (!published) published_at = null;
        else if (post?.published_at)
            published_at = db.toDate(post?.published_at);
        else published_at = new Date();

        await db
            .update('posts', { content, slug, published_at }, { id })
            .run(pool);
    } else
        await db
            .insert('posts', {
                id,
                user_id: user.sub,
                slug,
                content,
                created_at: new Date(),
                published_at: published ? new Date() : null
            })
            .run(pool);

    return res.status(200).send(null);
}

async function deletePostById(
    req: NextApiRequest,
    res: NextApiResponse<BlogPostContent | null>
) {
    const { id } = req.query;

    const session = getSession(req, res);
    let user: Claims = [];
    if (session) user = session.user;

    if (typeof id != 'string' || !isUUID(id)) return res.status(400).send(null);

    const post = await db.selectOne('posts', { id: id }).run(pool);
    if (!post) return res.status(404).send(null);
    if (!user || post.user_id != user.sub) return res.status(401).send(null);

    if (post) await db.deletes('posts', { id }).run(pool);

    return res.status(200).send(null);
}

const route = async (
    req: NextApiRequest,
    res: NextApiResponse<BlogPostContent | null>
) => {
    try {
        switch (req.method) {
            case 'GET':
                await getPostById(req, res);
                break;
            case 'PUT':
                await upsertPostById(req, res);
                break;
            case 'DELETE':
                await deletePostById(req, res);
                break;
            default:
                res.status(405).send(null);
                break;
        }
    } catch (e) {
        console.error('error while handling request', e);
        res.status(500).send(null);
    }
};

export default route;
