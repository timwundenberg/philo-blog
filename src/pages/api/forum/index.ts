import type { NextApiRequest, NextApiResponse } from 'next';
import { pool } from '../../../lib/db';
import * as db from 'zapatos/db';
import type * as s from 'zapatos/schema';
import {
    BlogPostFeedElement,
    BlogPostFeedSearch
} from '../models/blog-post-dto';
import { Claims, getSession } from '@auth0/nextjs-auth0';
import { getUsername } from '../../../lib/query/auth0/users';

async function getPostFeed(
    req: NextApiRequest,
    res: NextApiResponse<BlogPostFeedElement[] | null>
) {
    let search: BlogPostFeedSearch;
    try {
        search = JSON.parse(req.body);
    } catch {
        search = { page: 0, pageSize: 10 };
    }

    const session = getSession(req, res);
    let user: Claims = [];
    if (session) user = session.user;

    const limit = search.pageSize ? search.pageSize : 10;
    const offset = (search.page ? search.page : 0) * limit;

    const posts = await db.sql<s.posts.SQL, s.posts.Selectable[]>`
        SELECT ${'id'}, ${'user_id'}, ${'slug'}, ${'published_at'}, ${'updated_at'}
        FROM ${'posts'} 
        WHERE NOT ${'published_at'} IS NULL
           OR ${'user_id'} = ${db.param(user.sub)}
        ORDER BY ${'published_at'} DESC, ${'slug'} 
        LIMIT ${db.param(limit)}
        OFFSET ${db.param(offset)};`.run(pool);

    const result: BlogPostFeedElement[] = await Promise.all(
        posts.map(async (post): Promise<BlogPostFeedElement> => {
            return {
                id: post.id,
                userId: post.user_id,
                username: await getUsername(post.user_id),
                slug: post.slug,
                publishedAt: post.published_at?.toISOString() ?? null,
                updatedAt: post.updated_at?.toISOString() ?? null
            };
        })
    );
    return res.status(200).json(result);
}

const route = async (
    req: NextApiRequest,
    res: NextApiResponse<BlogPostFeedElement[] | null>
) => {
    try {
        switch (req.method) {
            case 'GET':
                await getPostFeed(req, res);
                break;
        }
    } catch (e) {
        console.error('error while handling request', e);
        res.status(500).send(null);
    }
};

export default route;
