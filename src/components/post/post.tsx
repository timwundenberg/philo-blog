import React, { FunctionComponent } from 'react';
import ReactMarkdown from 'react-markdown';

type Data = {
    content: string | null;
};

const Post: FunctionComponent<Data> = (props) => {
    return props.content ? (
        <ReactMarkdown>{props.content}</ReactMarkdown>
    ) : null;
};

export default Post;
