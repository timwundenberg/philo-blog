import Link from 'next/link';
import React, { FunctionComponent, useEffect, useState } from 'react';
import { usePosts } from '../../lib/query/posts';
import Loading from '../loading';

type FeedItemData = {
    username: string;
    id: string;
    slug: string;
    publishedAt: string | null;
};

const FeedItem: FunctionComponent<FeedItemData> = (params) => {
    const [date, setDate] = useState('');

    useEffect(() => {
        if (params.publishedAt != null) {
            const _date = new Date(params.publishedAt);
            setDate(_date.toLocaleDateString());
        }
    }, [params.publishedAt]);

    return (
        <article className="border mt-4 p-3">
            <Link href={`/forum/${params.id}`}>
                <a>
                    <span className="text-xl text-gray-900 block">
                        {params.slug}
                    </span>
                </a>
            </Link>
            {date != '' ? (
                <>
                    <span className="text-gray-400">Von </span>
                    <Link href={`/user/${params.username}`}>
                        <a>
                            <span className="text-gray-600">
                                {params.username}
                            </span>
                        </a>
                    </Link>
                    <span className="text-gray-400"> am </span>
                    <span className="text-gray-600">{date}</span>
                    <span className="text-gray-400"> publiziert. </span>
                </>
            ) : (
                <>
                    <span className="text-gray-400">Unveröffentlicht</span>
                </>
            )}
        </article>
    );
};

const NewPost: FunctionComponent = () => {
    return (
        <article className="border mt-4 p-3">
            <Link href={`/forum/new`}>
                <a>
                    <span className="text-xl text-gray-900 block">Neu</span>
                </a>
            </Link>
        </article>
    );
};

const PostFeed = () => {
    const { data, isLoading } = usePosts();

    return (
        <>
            <h2>Posts</h2>
            <NewPost />
            {data?.map((post) => (
                <FeedItem key={post.id} {...post} />
            ))}
            <Loading show={isLoading} />
        </>
    );
};

export default PostFeed;
