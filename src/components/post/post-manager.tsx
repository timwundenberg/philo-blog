import React, {
    FunctionComponent,
    useEffect,
    useLayoutEffect,
    useState
} from 'react';
import Loading from '../loading';
import BlogPostEdit from './post-edit';
import Container from '../form/container';
import { EditMode } from './post-helper';
import {
    useMutateDeletePost,
    useMutateSavePost,
    usePostById
} from '../../lib/query/posts';
import Button from '../form/btn';
import BlogPostView from './post-view';
import EnsureUser from '../auth/ensure-user';
import { useRouter } from 'next/router';
import { v4 as uuidv4 } from 'uuid';

type Data = {
    mode: EditMode;
    postId: string;
};

enum ManagerMode {
    View,
    Edit
}

const BlogPostManager: FunctionComponent<Data> = (param) => {
    const { data, isLoading } = usePostById(param.postId);
    const router = useRouter();
    const [content, setContent] = useState('');
    const [contentEdited, setContentEdited] = useState('');
    const [publishedEdited, setpublishedEdited] = useState(false);
    const [published, setPublished] = useState(false);
    const [slug, setSlug] = useState('');
    const [slugEdited, setSlugEdited] = useState('');
    const [mode, setMode] = useState<ManagerMode>(ManagerMode.View);

    useEffect(() => {
        setContent(data?.content ?? '');
        setSlug(data?.slug ?? '');
        setPublished(data?.published ?? false);
    }, [data?.content, data?.slug, data?.published]);
    useLayoutEffect(() => {
        setMode(
            param.mode == EditMode.Edit ? ManagerMode.View : ManagerMode.Edit
        );
    }, [param.mode]);

    const mutationSave = useMutateSavePost();
    const mutationDelete = useMutateDeletePost();

    const doEdit = () => {
        setSlugEdited(slug);
        setContentEdited(content);
        setpublishedEdited(published);
        setMode(ManagerMode.Edit);
    };
    const doDelete = async () => {
        (await mutationDelete).mutate(param.postId);
        await router.push(`/forum`);
    };

    const saveEdit = async () => {
        const newPostId = uuidv4();

        (await mutationSave).mutate({
            id: param.mode == EditMode.Insert ? newPostId : param.postId,
            slug: slugEdited,
            content: contentEdited,
            published: publishedEdited
        });

        if (param.mode == EditMode.Insert)
            await router.push(`/forum/${newPostId}`);
        else setMode(ManagerMode.View);
    };
    const cancelEdit = () => {
        if (param.mode == EditMode.Insert) router.back();
        else setMode(ManagerMode.View);
    };

    return (
        <Container className="flex">
            <div className="flex-1">
                {isLoading ? (
                    <Loading show />
                ) : mode == ManagerMode.View ? (
                    <BlogPostView
                        content={content}
                        slug={slug}
                        showComments={mode == ManagerMode.View}
                    />
                ) : (
                    <BlogPostEdit
                        onChange={(slug, content, published) => {
                            setSlugEdited(slug);
                            setContentEdited(content);
                            setpublishedEdited(published);
                        }}
                        slug={slugEdited}
                        content={contentEdited}
                        published={publishedEdited}
                        mode={EditMode.Insert}
                    />
                )}
            </div>

            <aside className="self-start border rounded p-4">
                <h5 className="text-xl font-semibold">Weiteres</h5>
                <p>Likes</p>
                <p>Views</p>
                <p># Kommentare</p>
                <EnsureUser userId={data?.userId}>
                    {mode == ManagerMode.View ? (
                        <>
                            <Button primary onClick={() => doEdit()}>
                                Bearbeiten
                            </Button>
                            <Button danger onClick={() => doDelete()}>
                                Löschen
                            </Button>
                        </>
                    ) : (
                        <>
                            <Button primary onClick={() => saveEdit()}>
                                Speichern
                            </Button>
                            <Button onClick={() => cancelEdit()}>
                                Abbrechen
                            </Button>
                        </>
                    )}
                </EnsureUser>
            </aside>
        </Container>
    );
};

export default BlogPostManager;
