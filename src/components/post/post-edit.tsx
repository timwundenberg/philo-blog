import React, { FunctionComponent, useState } from 'react';
import { EditMode } from './post-helper';

interface Data {
    mode: EditMode;
    slug: string;
    content: string;
    published: boolean;
    onChange: (aSulg: string, aContent: string, published: boolean) => void;
}

const BlogPostEdit: FunctionComponent<Data> = (params) => {
    const [content, setContent] = useState(params.content);
    const [slug, setSlug] = useState(params.slug);
    const [published, setPublished] = useState(params.published);

    return (
        <form className="mr-5">
            <div className="md:flex md:items-start mb-6">
                <div className="md:w-1/4">
                    <label
                        className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4"
                        htmlFor="slug"
                    >
                        Titel
                    </label>
                </div>
                <div className="md:w-3/4">
                    <input
                        className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        id="slug"
                        type="text"
                        placeholder="Die wundersame Geschichte eines ..."
                        value={slug}
                        onChange={(e) => {
                            setSlug(e.target.value);
                            params.onChange(e.target.value, content, published);
                        }}
                    />
                </div>
            </div>
            <div className="md:flex md:items-start mb-6">
                <div className="md:w-1/4"></div>
                <label className="md:w-3/4 block text-gray-500 font-bold">
                    <input
                        className="mr-2 leading-tight"
                        type="checkbox"
                        onChange={(e) => {
                            setPublished(e.target.checked);
                            params.onChange(slug, content, e.target.checked);
                        }}
                        checked={published}
                    />
                    <span className="text-sm">Veröffentlicht</span>
                </label>
            </div>
            <div className="md:flex md:items-start mb-6">
                <div className="md:w-1/4">
                    <label
                        className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4"
                        htmlFor="content"
                    >
                        Inhalt
                    </label>
                </div>
                <div className="md:w-3/4">
                    <textarea
                        className="h-20 bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        id="content"
                        onChange={(e) => {
                            setContent(e.target.value);
                            params.onChange(slug, e.target.value, published);
                        }}
                        value={content}
                    />
                </div>
            </div>
        </form>
    );
};

export default BlogPostEdit;
