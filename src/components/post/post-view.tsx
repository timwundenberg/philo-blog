import React, { FunctionComponent } from 'react';
import ReactMarkdown from 'react-markdown';

interface Data {
    content: string;
    slug: string;
    showComments: boolean;
}

const BlogPostView: FunctionComponent<Data> = (params) => {
    return (
        <article className="flex-1 m-10 mt-0">
            <h2 className="text-6xl font-bold tracking-tighter">
                {params.slug}
            </h2>
            <ReactMarkdown className="mt-10 prose">
                {params.content}
            </ReactMarkdown>
            {params.showComments && (
                <>
                    <h2 className="text-3xl font-bold mt-10">Kommentare</h2>
                    <h2 className="text-slate-700">
                        Es sind keine Kommentare vorhanden!
                    </h2>
                </>
            )}
        </article>
    );
};

export default BlogPostView;
