import { useUser } from '@auth0/nextjs-auth0';
import React from 'react';
import SignOutButton from './signout-btn';

function Profile() {
    const { user } = useUser();

    return (
        <div>
            <p>{user?.name}</p>
            <SignOutButton />
        </div>
    );
}

export default Profile;
