import Link from 'next/link';
import React from 'react';

function SignOutButton() {
    return <Link href="/api/auth/logout">Log out</Link>;
}

export default SignOutButton;
