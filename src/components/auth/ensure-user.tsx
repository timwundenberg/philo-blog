import { useUser } from '@auth0/nextjs-auth0';
import { FunctionComponent } from 'react';

type Data = {
    userId?: string;
};

const EnsureUser: FunctionComponent<Data> = (params) => {
    const { user } = useUser();
    return <>{user && <>{params.children}</>}</>;
};
export default EnsureUser;
