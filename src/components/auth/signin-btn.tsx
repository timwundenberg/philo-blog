import Link from 'next/link';
import React from 'react';

function SignInButton() {
    return <Link href="/api/auth/login">Sign in</Link>;
}

export default SignInButton;
