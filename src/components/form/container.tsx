import React, { FunctionComponent } from 'react';

type Data = {
    className?: string;
};

const Container: FunctionComponent<Data> = (props) => {
    return (
        <div className={`container mx-auto max-w-5xl my-7 ${props.className}`}>
            {props.children}
        </div>
    );
};

export default Container;
