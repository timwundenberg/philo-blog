import React, { FunctionComponent } from 'react';

type Data = {
    onClick?: () => void;
    primary?: boolean;
    danger?: boolean;
    icon?: string;
    className?: string;
};

const Button: FunctionComponent<Data> = (props) => {
    return (
        <button
            onClick={props.onClick}
            className={`p-2 text-lg border rounded-lg min-w-[5rem]
                ${props.primary ? 'bg-amber-300' : ''}
                ${props.danger ? 'bg-red-400' : ''}
                ${props.className}`}
        >
            {props.children}
        </button>
    );
};

export default Button;
