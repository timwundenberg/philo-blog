import Link from 'next/link';

const Footer = () => {
    return (
        <footer className="justify-self-end bg-gray-200 text-center py-5">
            <p className="text-gray-700">Philo-Blog</p>
            <nav className="flex gap-6 justify-center mt-2">
                <Link href="/imprint">
                    <a className="text-gray-700">Impressum</a>
                </Link>
                <Link href="/privacy">
                    <a className="text-gray-700">Datenschutz</a>
                </Link>
            </nav>
        </footer>
    );
};

export default Footer;
