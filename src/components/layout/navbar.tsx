import Link from 'next/link';
import { useUser } from '@auth0/nextjs-auth0';
import Loading from '../loading';
import Profile from '../auth/profile';
import SignInButton from '../auth/signin-btn';
import Image from 'next/image';

const Navbar = () => {
    const { user, isLoading } = useUser();
    return (
        <>
            <header className="flex items-center justify-between px-32 py-4 bg-amber-200">
                <Link href="/">
                    <a className="flex justify-center align-middle text-center">
                        <Image
                            src="/static/logo.svg"
                            alt="Logo of the website"
                            width="40"
                            height="0"
                        />

                        <h2 className="text-xl font-bold">Philoferie</h2>
                    </a>
                </Link>
                <nav>
                    <div className="flex gap-6">
                        <Link href="/">Home</Link>
                        <Link href="/forum">Forum</Link>
                        <Link href="/about">Über</Link>
                    </div>
                </nav>
                <div>
                    {isLoading ?? <Loading show />}
                    {user ? <Profile /> : <SignInButton />}
                </div>
            </header>
        </>
    );
};

export default Navbar;
