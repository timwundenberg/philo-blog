import { FunctionComponent } from 'react';
import Footer from './footer';
import Navbar from './navbar';

const Layout: FunctionComponent = (props) => {
    return (
        <div className="flex flex-col min-h-screen">
            <Navbar />
            <main className="flex-1 ">{props.children}</main>
            <Footer />
        </div>
    );
};

export default Layout;
