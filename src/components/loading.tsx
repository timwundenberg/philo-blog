import React, { FunctionComponent } from 'react';

type Data = {
    show: boolean;
};

const Loading: FunctionComponent<Data> = (props) => {
    return props.show ? <div>Loading</div> : null;
};

export default Loading;
