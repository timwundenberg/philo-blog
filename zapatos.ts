import * as zg from 'zapatos/generate';
import { config } from 'dotenv';

config();

const zapCfg: zg.Config = {
    db: {
        application_name: 'next.js/philo-blog/zapatos',

        host: process.env.PGHOST,
        database: process.env.PGDATABASE,
        user: process.env.PGUSER,
        password: process.env.PGPASSWORD,
        port: process.env.PGPORT ? +process.env.PGPORT : 5432
    },
    outDir: './src',
    progressListener: true
};

void zg.generate(zapCfg);
