# Philo-Blog

## Introduction

This project is a forum to exchange and discuss philosophoical thoughts.

### Target audience

The target audience are philosophical interested german people.

## Getting Started

The following applications are required on your computer:

-   yarn
-   node v16
-   docker-compose

To setup the environment for philo-blog run the following commands:

```bash
yarn install
yarn prepare
docker-compose up -d
sudo chown -R 5050:5050 /var/lib/philo-blog/data-admin/
```

Run dev server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

To view the databse, can open [http://localhost:8080](http://localhost:8080) and setup the db connection. You can use the credentials stored in the .env file. But there is the exception of the host, which has to be set to "postgres". This is due to the docker-container setup.

When changing the database schema and adding a migration, then you have update the type definitions by running

```
yarn zapatos
```
